var app = getApp(), util = require("../../utils/util.js"), APP_ID = "", APP_SECRET = "", OPEN_ID = "", SESSION_KEY = "";

Page({
    data: {
        xzggindex: "0",
        showModal: !1,
        boxfre: !0,
        changeHidden1: !0,
        changeHidden: !0,
        toastHidden: !0,
        selected1: !0,
        selected2: !1,
        selected3: !1,
        showview: !0,
        hidden1: !1,
        hidden2: !0,
        hidden3: !0,
        catalogSelect: 0,
        store: [],
        http: [],
        showView: !1,
        close: !1,
        login: [],
        rest: "",
        start_at: "",
        conditions: "",
        preferential: "",
        dishes: [],
        link: "",
        toView: "0",
        store_name: "",
        scrollTop: 100,
        totalPrice: 0,
        totalCount: 0,
        carArray: [],
        freight: 0,
        payDesc: 0,
        userInfo: {},
        parentIndex: 0,
        url: "",
        hidden: !1,
        curNav: 1,
        curIndex: 0,
        cart: [],
        cartTotal: 0,
        one: 1,
        ping: "",
        hdnum: 0,
        star1: [ {
            url: "../images/full-star.png"
        }, {
            url: "../images/no-star.png"
        }, {
            url: "../images/no-star.png"
        }, {
            url: "../images/no-star.png"
        }, {
            url: "../images/no-star.png"
        } ],
        star2: [ {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        }, {
            url: "../images/no-star.png"
        }, {
            url: "../images/no-star.png"
        }, {
            url: "../images/no-star.png"
        } ],
        star3: [ {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        }, {
            url: "../images/no-star.png"
        }, {
            url: "../images/no-star.png"
        } ],
        star4: [ {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        }, {
            url: "../images/no-star.png"
        } ],
        star5: [ {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        }, {
            url: "../images/full-star.png"
        } ]
    },
    ycgg: function() {
        this.setData({
            showModal: !1
        });
    },
    onLoad: function(t) {
        var a = this, e = wx.getStorageSync("bqxx");
        console.log(e), this.setData({
            bqxx: e
        }), wx.getSystemInfo({
            success: function(t) {
                console.log(t.windowWidth), console.log(t.windowHeight), a.setData({
                    height: t.windowHeight / t.windowWidth * 750 - 500
                });
            }
        }), wx.getUserInfo({
            success: function(t) {
                console.log(t.userInfo);
                var e = t.userInfo;
                a.setData({
                    nickName: e.nickName,
                    avatarUrl: e.avatarUrl
                });
            },
            fail: function() {
                wx.showModal({
                    title: "警告",
                    content: "您点击了拒绝授权,无法正常显示个人信息,点击确定重新获取授权。",
                    showCancel: !1,
                    success: function(t) {
                        t.confirm && wx.openSetting({
                            success: function(t) {
                                t.authSetting["scope.userInfo"] ? wx.getUserInfo({
                                    success: function(t) {
                                        console.log(t.userInfo);
                                        var e = t.userInfo;
                                        a.setData({
                                            nickName: e.nickName,
                                            avatarUrl: e.avatarUrl
                                        });
                                    }
                                }) : a.setData({
                                    nickName: "",
                                    avatarUrl: ""
                                });
                            },
                            fail: function(t) {}
                        });
                    }
                });
            },
            complete: function(t) {}
        }), wx.login({
            success: function(t) {
                var e = t.code;
                wx.setStorageSync("code", t.code), app.util.request({
                    url: "entry/wxapp/openid",
                    cachetime: "0",
                    data: {
                        code: e
                    },
                    success: function(t) {
                        console.log(t), wx.setStorageSync("key", t.data.session_key), wx.setStorageSync("openid", t.data.openid);
                        var e = t.data.openid;
                        console.log(e), "" == e ? wx.showToast({
                            title: "没有获取到openid",
                            icon: "",
                            image: "",
                            duration: 1e3,
                            mask: !0,
                            success: function(t) {},
                            fail: function(t) {},
                            complete: function(t) {}
                        }) : app.util.request({
                            url: "entry/wxapp/Login",
                            cachetime: "0",
                            data: {
                                openid: e,
                                img: a.data.avatarUrl,
                                name: a.data.nickName
                            },
                            success: function(t) {
                                console.log(t), wx.setStorageSync("users", t.data), app.util.request({
                                    url: "entry/wxapp/New",
                                    cachetime: "0",
                                    data: {
                                        user_id: t.data.id,
                                        store_id: getApp().sjid
                                    },
                                    success: function(t) {
                                        console.log(t), wx.setStorageSync("new_user", t.data);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }), console.log(t);
        var s = Number(t.type);
        1 == s ? console.log("用户选择的是店内点餐") : console.log("用户选择的是外卖点餐"), a.setData({
            types: s
        });
        var o = decodeURIComponent(t.scene).split(",");
        console.log(o), "undefined" != o && (getApp().sjid = o[1], this.setData({
            types: 1,
            tableid: o[0]
        }), app.util.request({
            url: "entry/wxapp/Store",
            cachetime: "0",
            data: {
                id: getApp().sjid
            },
            success: function(t) {
                console.log(t), "1" == t.data.is_czztpd ? app.util.request({
                    url: "entry/wxapp/Zhuohao",
                    cachetime: "0",
                    data: {
                        id: o[0]
                    },
                    success: function(t) {
                        console.log(t), "0" == t.data.status ? (wx.showModal({
                            title: "提示",
                            content: "桌位信息：" + t.data.type_name + "--" + t.data.table_name,
                            showCancel: !1,
                            success: function(t) {},
                            fail: function(t) {},
                            complete: function(t) {}
                        }), a.setData({
                            kt: !1
                        })) : (wx.showModal({
                            title: "提示",
                            content: "此桌已开台暂不能点餐,请选择其他座位",
                            showCancel: !1,
                            success: function(t) {},
                            fail: function(t) {},
                            complete: function(t) {}
                        }), setTimeout(function() {
                            wx.navigateBack({});
                        }, 1e3), a.setData({
                            kt: !0
                        }));
                    }
                }) : app.util.request({
                    url: "entry/wxapp/Zhuohao",
                    cachetime: "0",
                    data: {
                        id: o[0]
                    },
                    success: function(t) {
                        console.log(t), wx.showModal({
                            title: "提示",
                            content: "桌位信息：" + t.data.type_name + "--" + t.data.table_name,
                            showCancel: !1,
                            success: function(t) {},
                            fail: function(t) {},
                            complete: function(t) {}
                        }), a.setData({
                            kt: !1
                        });
                    }
                });
            }
        })), t.showview, t.showView, a.reload();
    },
    reload: function(t) {
        var a = this, e = util.formatTime(new Date()).slice(11, 16);
        this.data.store_name;
        wx.showShareMenu({
            withShareTicket: !0
        }), app.util.request({
            url: "entry/wxapp/Store",
            cachetime: "0",
            data: {
                id: getApp().sjid
            },
            success: function(t) {
                "" != t.data.wm_name && 2 == a.data.types && wx.setNavigationBarTitle({
                    title: t.data.wm_name
                }), "" != t.data.dn_name && 1 == a.data.types && wx.setNavigationBarTitle({
                    title: t.data.dn_name
                }), wx.setNavigationBarColor({
                    frontColor: "#ffffff",
                    backgroundColor: t.data.color
                }), app.util.request({
                    url: "entry/wxapp/zhuanh",
                    cachetime: "0",
                    data: {
                        op: t.data.coordinates
                    },
                    success: function(t) {
                        console.log(t), console.log(t.data.locations[0].lat + "," + t.data.locations[0].lng), 
                        a.setData({
                            sjdzlat: t.data.locations[0].lat,
                            sjdzlng: t.data.locations[0].lng
                        });
                    }
                }), console.log(t), app.util.request({
                    url: "entry/wxapp/Reduction",
                    cachetime: "0",
                    data: {
                        id: getApp().sjid
                    },
                    success: function(e) {
                        console.log(e);
                        for (var s = [], o = [], i = 0; i < e.data.length; i++) "2" != e.data[i].type && "3" != e.data[i].type || s.push(e.data[i]), 
                        "1" != e.data[i].type && "3" != e.data[i].type || o.push(e.data[i]);
                        if (1 == a.data.types) {
                            var d = s;
                            a.setData({
                                mj: s
                            });
                        }
                        if (2 == a.data.types) {
                            d = o;
                            a.setData({
                                mj: o
                            });
                        }
                        0 != d.length && "1" == t.data.xyh_open ? a.setData({
                            hdnum: 2
                        }) : 0 != d.length && "1" != t.data.xyh_open || 0 == d.length && "1" == t.data.xyh_open ? a.setData({
                            hdnum: 1
                        }) : a.setData({
                            hdnum: 0
                        });
                    }
                });
                var s = t.data.id, o = t.data.time, i = t.data.time2, d = t.data.time3, n = t.data.time4, r = t.data.is_rest;
                console.log("当前的系统时间为" + e), console.log("商家的营业时间从" + o + "至" + i, d + "至" + n), 
                1 == r ? console.log("商家正在休息" + r) : console.log("商家正在营业" + r), n > o ? e > o && e < i || e > d && e < n ? (console.log("商家正常营业"), 
                a.setData({
                    time: 1
                })) : e < o || e > i && e < d ? (console.log("商家还没开店呐，稍等一会儿可以吗？"), a.setData({
                    time: 2
                })) : e > n && (console.log("商家以及关店啦，明天再来吧"), a.setData({
                    time: 3
                })) : n < o && (e > o && e < i || e > d && e > n || e < d && e < n ? (console.log("商家正常营业"), 
                a.setData({
                    time: 1
                })) : e < o || e > i && e < d ? (console.log("商家还没开店呐，稍等一会儿可以吗？"), a.setData({
                    time: 2
                })) : e > n && (console.log("商家以及关店啦，明天再来吧"), a.setData({
                    time: 3
                }))), console.log("商家的id为" + s), app.util.request({
                    url: "entry/wxapp/Score",
                    cachetime: "0",
                    data: {
                        seller_id: s
                    },
                    success: function(t) {
                        console.log(t);
                        var e = t.data;
                        e = e.toFixed(1), console.log(e), a.setData({
                            score: e
                        });
                    }
                }), app.util.request({
                    url: "entry/wxapp/StorePl",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    cachetime: "0",
                    data: {
                        id: s
                    },
                    success: function(t) {
                        console.log(t.data);
                        var e = t.data.length;
                        console.log(e);
                        for (var s = 0; s < t.data.length; s++) t.data[s].score = t.data[s].score.slice(0, 2);
                        console.log(t.data), a.setData({
                            ping: t.data
                        });
                    }
                }), a.setData({
                    store: t.data,
                    rest: r,
                    color: t.data.color,
                    seller_id: s,
                    start_at: t.data.start_at
                });
            }
        }), app.util.request({
            url: "entry/wxapp/Url",
            cachetime: "0",
            success: function(t) {
                a.setData({
                    url: t.data
                });
            }
        });
        var s = (a = this).data.types;
        console.log(s), app.util.request({
            url: "entry/wxapp/Dishes",
            cachetime: "0",
            data: {
                id: getApp().sjid,
                dishes_type: s
            },
            success: function(t) {
                console.log(t.data);
                for (var e = 0; e < t.data.length; e++) for (var s = 0; s < t.data[e].goods.length; s++) t.data[e].goods[s].xs_num = Number(t.data[e].goods[s].xs_num), 
                t.data[e].goods[s].sit_ys_num = Number(t.data[e].goods[s].sit_ys_num);
                console.log(t.data), a.setData({
                    dishes: t.data
                });
            }
        });
    },
    selected1: function(t) {
        this.setData({
            selected2: !1,
            selected3: !1,
            selected1: !0,
            hidden1: !1,
            hidden2: !0,
            hidden3: !0
        });
    },
    selected2: function(t) {
        this.setData({
            selected1: !1,
            selected2: !0,
            selected3: !1,
            hidden1: !0,
            hidden2: !1,
            hidden3: !0
        });
    },
    selected3: function(t) {
        this.setData({
            selected1: !1,
            selected2: !1,
            selected3: !0,
            hidden1: !0,
            hidden2: !0,
            hidden3: !1
        });
    },
    onReady: function() {
        console.log(this.data.types);
    },
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        this.reload(), wx.stopPullDownRefresh();
    },
    onReachBottom: function() {},
    onShareAppMessage: function() {},
    call_phone: function() {
        wx.makePhoneCall({
            phoneNumber: this.data.store.tel
        });
    },
    tomap: function(t) {
        wx.openLocation({
            latitude: this.data.sjdzlat,
            longitude: this.data.sjdzlng,
            name: this.data.store.name,
            address: this.data.store.address
        });
    },
    tzsjhj: function(t) {
        console.log(t.currentTarget.dataset.sjid), wx.navigateTo({
            url: "../info/sjhj"
        });
    },
    selectMenu: function(t) {
        var a = t.currentTarget.dataset.itemIndex;
        this.setData({
            toView: "order" + a.toString(),
            catalogSelect: t.currentTarget.dataset.itemIndex
        }), console.log("order" + a.toString());
    },
    close: function() {
        this.setData({
            showView: !this.data.showView
        });
    },
    zwkc: function() {
        wx.showToast({
            title: "没有库存了"
        });
    },
    addShopCart: function(t) {
        console.log(this.data.dishes), console.log(this.data.carArray), console.log(t.currentTarget.dataset);
        var a = t.currentTarget.dataset.itemIndex, e = t.currentTarget.dataset.parentindex, s = t.currentTarget.dataset.gwcindex, o = t.currentTarget.dataset.ggindex;
        if (null != o) {
            this.data.dishes[e].goods[a].one++, this.data.dishes[e].goods[a].gg[o].num++;
            var i = "a" + a + "b" + e + "c" + this.data.dishes[e].goods[a].gg[o].id;
            if (2 == this.data.types) var d = this.data.dishes[e].goods[a].gg[o].cost; else d = this.data.dishes[e].goods[a].gg[o].cost;
            var n = this.data.dishes[e].goods[a].box_fee;
            console.log("餐盒费是：" + n);
            var r = Number(this.data.dishes[e].goods[a].num), l = this.data.dishes[e].goods[a].gg[o].num, c = this.data.dishes[e].goods[a].name + this.data.dishes[e].goods[a].gg[o].name, h = this.data.dishes[e].goods[a].id, g = this.data.dishes[e].goods[a].img, u = this.data.store, p = {
                ggindex: o,
                money: d,
                num: l,
                kc: r,
                id: h,
                mark: i,
                name: c,
                index: a,
                parentIndex: e,
                icon: g,
                store: u,
                box_fee: n,
                allmoney: (d * l).toFixed(2)
            };
            (m = this.data.carArray.filter(function(t) {
                return t.mark != i;
            })).splice(s, 0, p), console.log(m), this.setData({
                shop_cart: m,
                carArray: m,
                dishes: this.data.dishes
            }), console.log(this.data.carArray), this.calTotalPrice(), this.setData({
                payDesc: this.payDesc()
            });
        } else {
            console.log(o), this.data.dishes[e].goods[a].one++;
            i = "a" + a + "b" + e;
            if (2 == this.data.types) d = this.data.dishes[e].goods[a].wm_money; else d = this.data.dishes[e].goods[a].money;
            n = this.data.dishes[e].goods[a].box_fee;
            console.log("餐盒费是：" + n);
            var m;
            r = Number(this.data.dishes[e].goods[a].num), l = this.data.dishes[e].goods[a].one, 
            c = this.data.dishes[e].goods[a].name, h = this.data.dishes[e].goods[a].id, g = this.data.dishes[e].goods[a].img, 
            u = this.data.store, p = {
                money: d,
                num: l,
                kc: r,
                id: h,
                mark: i,
                name: c,
                index: a,
                parentIndex: e,
                icon: g,
                store: u,
                box_fee: n,
                allmoney: (d * l).toFixed(2)
            };
            (m = this.data.carArray.filter(function(t) {
                return t.mark != i;
            })).splice(s, 0, p), console.log(m), this.setData({
                shop_cart: m,
                carArray: m,
                dishes: this.data.dishes
            }), console.log(this.data.dishes), this.calTotalPrice(), this.setData({
                payDesc: this.payDesc()
            });
        }
    },
    decreaseShopCart: function(t) {
        console.log(this.data.dishes), console.log(this.data.carArray), console.log(t.currentTarget.dataset);
        var a = t.currentTarget.dataset.itemIndex, e = t.currentTarget.dataset.parentindex, s = t.currentTarget.dataset.gwcindex, o = t.currentTarget.dataset.ggindex;
        if (null != o) {
            this.data.dishes[e].goods[a].one--, this.data.dishes[e].goods[a].gg[o].num--;
            var i = "a" + a + "b" + e + "c" + this.data.dishes[e].goods[a].gg[o].id;
            if (2 == this.data.types) var d = this.data.dishes[e].goods[a].gg[o].cost; else d = this.data.dishes[e].goods[a].gg[o].cost;
            var n = this.data.dishes[e].goods[a].box_fee;
            console.log("餐盒费是：" + n);
            var r = Number(this.data.dishes[e].goods[a].num), l = this.data.dishes[e].goods[a].gg[o].num, c = this.data.dishes[e].goods[a].name + this.data.dishes[e].goods[a].gg[o].name, h = this.data.dishes[e].goods[a].id, g = this.data.dishes[e].goods[a].img, u = this.data.store, p = {
                ggindex: o,
                money: d,
                num: l,
                kc: r,
                id: h,
                mark: i,
                name: c,
                index: a,
                parentIndex: e,
                icon: g,
                store: u,
                box_fee: n,
                allmoney: (d * l).toFixed(2)
            };
            (m = this.data.carArray.filter(function(t) {
                return t.mark != i;
            })).splice(s, 0, p), console.log(m), this.setData({
                shop_cart: m,
                carArray: m,
                dishes: this.data.dishes
            }), console.log(this.data.carArray), this.calTotalPrice(), this.setData({
                payDesc: this.payDesc()
            });
        } else {
            console.log(o), this.data.dishes[e].goods[a].one--;
            i = "a" + a + "b" + e;
            if (2 == this.data.types) d = this.data.dishes[e].goods[a].wm_money; else d = this.data.dishes[e].goods[a].money;
            n = this.data.dishes[e].goods[a].box_fee;
            console.log("餐盒费是：" + n);
            var m;
            r = Number(this.data.dishes[e].goods[a].num), l = this.data.dishes[e].goods[a].one, 
            c = this.data.dishes[e].goods[a].name, h = this.data.dishes[e].goods[a].id, g = this.data.dishes[e].goods[a].img, 
            u = this.data.store, p = {
                money: d,
                num: l,
                kc: r,
                id: h,
                mark: i,
                name: c,
                index: a,
                parentIndex: e,
                icon: g,
                store: u,
                box_fee: n,
                allmoney: (d * l).toFixed(2)
            };
            (m = this.data.carArray.filter(function(t) {
                return t.mark != i;
            })).splice(s, 0, p), this.setData({
                shop_cart: m,
                carArray: m,
                dishes: this.data.dishes
            }), this.calTotalPrice(), this.setData({
                payDesc: this.payDesc()
            }), console.log(this.data.dishes);
        }
    },
    decreaseCart: function(t) {
        console.log("你点击了减号");
        var a = this;
        console.log(this.data);
        var e = t.currentTarget.dataset.itemIndex, s = t.currentTarget.dataset.parentindex;
        console.log(e, s), console.log(s, a.data.dishes[s].goods[e].id), app.util.request({
            url: "entry/wxapp/DishesGg",
            cachetime: "0",
            data: {
                dishes_id: a.data.dishes[s].goods[e].id
            },
            success: function(t) {
                if (console.log(t), 0 != t.data.length) wx.showModal({
                    title: "提示",
                    showCancel: !1,
                    content: "多规格商品只能在购物车删除哦"
                }); else {
                    a.data.dishes[s].goods[e].one--;
                    var o = "a" + e + "b" + s;
                    if (2 == a.data.types) var i = a.data.dishes[s].goods[e].wm_money; else i = a.data.dishes[s].goods[e].money;
                    var d = a.data.dishes[s].goods[e].box_fee;
                    console.log("餐盒费是：" + d);
                    var n = Number(a.data.dishes[s].goods[e].num), r = a.data.dishes[s].goods[e].one, l = a.data.dishes[s].goods[e].name, c = a.data.dishes[s].goods[e].id, h = a.data.dishes[s].goods[e].img, g = a.data.store, u = {
                        money: i,
                        num: r,
                        kc: n,
                        id: c,
                        mark: o,
                        name: l,
                        index: e,
                        parentIndex: s,
                        icon: h,
                        store: g,
                        box_fee: d,
                        allmoney: (i * r).toFixed(2)
                    }, p = a.data.carArray.filter(function(t) {
                        return t.mark != o;
                    });
                    p.splice(e, 0, u), a.setData({
                        shop_cart: p,
                        carArray: p,
                        dishes: a.data.dishes
                    }), a.calTotalPrice(), a.setData({
                        payDesc: a.payDesc()
                    }), console.log(a.data.dishes);
                }
            }
        });
    },
    xzggClick: function(t) {
        this.setData({
            xzggindex: t.currentTarget.id
        });
    },
    xhl: function() {
        var t = this.data.zindex, a = this.data.findex;
        console.log(this.data.zindex, this.data.findex), console.log(this.data.dishes), 
        this.data.dishes[a].goods[t].one++, this.data.dishes[a].goods[t].gg[this.data.xzggindex].num++;
        var e = "a" + t + "b" + a + "c" + this.data.gg[this.data.xzggindex].id, s = this.data.xzggindex;
        if (2 == this.data.types) var o = this.data.gg[this.data.xzggindex].cost; else o = this.data.gg[this.data.xzggindex].cost;
        var i = this.data.dishes[a].goods[t].box_fee;
        console.log("餐盒费是：" + i);
        var d = Number(this.data.dishes[a].goods[t].num), n = this.data.dishes[a].goods[t].gg[this.data.xzggindex].num, r = this.data.ggbt + this.data.gg[this.data.xzggindex].name, l = this.data.dishes[a].goods[t].id, c = this.data.dishes[a].goods[t].img, h = this.data.store, g = {
            ggindex: s,
            money: o,
            num: n,
            kc: d,
            id: l,
            mark: e,
            name: r,
            index: t,
            parentIndex: a,
            icon: c,
            store: h,
            box_fee: i,
            allmoney: (o * n).toFixed(2)
        }, u = this.data.carArray.filter(function(t) {
            return t.mark != e;
        });
        u.splice(t, 0, g), console.log(u), this.setData({
            shop_cart: u,
            carArray: u,
            dishes: this.data.dishes
        }), this.calTotalPrice(), this.setData({
            payDesc: this.payDesc()
        }), this.setData({
            showModal: !1,
            xzggindex: 0
        });
    },
    addCart: function(t) {
        console.log(this.data);
        var a = this, e = t.currentTarget.dataset.itemIndex, s = t.currentTarget.dataset.parentindex;
        this.setData({
            zindex: e,
            findex: s
        }), console.log(s, a.data.dishes[s].goods[e].id), app.util.request({
            url: "entry/wxapp/DishesGg",
            cachetime: "0",
            data: {
                dishes_id: a.data.dishes[s].goods[e].id
            },
            success: function(t) {
                if (console.log(t), 0 != t.data.length) null == a.data.dishes[s].goods[e].gg ? (a.setData({
                    showModal: !0,
                    gg: t.data,
                    ggbt: a.data.dishes[s].goods[e].name
                }), a.data.dishes[s].goods[e].gg = t.data, a.setData({
                    dishes: a.data.dishes
                }), console.log(a.data.dishes)) : (a.setData({
                    showModal: !0,
                    gg: t.data,
                    ggbt: a.data.dishes[s].goods[e].name
                }), console.log(a.data.dishes)); else {
                    a.data.dishes[s].goods[e].one++;
                    var o = "a" + e + "b" + s;
                    if (2 == a.data.types) var i = a.data.dishes[s].goods[e].wm_money; else i = a.data.dishes[s].goods[e].money;
                    var d = a.data.dishes[s].goods[e].box_fee;
                    console.log("餐盒费是：" + d);
                    var n = Number(a.data.dishes[s].goods[e].num), r = a.data.dishes[s].goods[e].one, l = a.data.dishes[s].goods[e].name, c = a.data.dishes[s].goods[e].id, h = a.data.dishes[s].goods[e].img, g = a.data.store, u = {
                        money: i,
                        num: r,
                        kc: n,
                        id: c,
                        mark: o,
                        name: l,
                        index: e,
                        parentIndex: s,
                        icon: h,
                        store: g,
                        box_fee: d,
                        allmoney: (i * r).toFixed(2)
                    }, p = a.data.carArray.filter(function(t) {
                        return t.mark != o;
                    });
                    p.splice(e, 0, u), console.log(p), a.setData({
                        shop_cart: p,
                        carArray: p,
                        dishes: a.data.dishes
                    }), console.log(a.data.dishes), a.calTotalPrice(), a.setData({
                        payDesc: a.payDesc()
                    });
                }
            }
        });
    },
    calTotalPrice: function() {
        for (var t = this.data.carArray, a = 0, e = 0, s = 0, o = 0; o < t.length; o++) 2 == this.data.types ? (a += t[o].money * t[o].num + t[o].box_fee * t[o].num, 
        s += t[o].num, e += t[o].box_fee * t[o].num) : (a += t[o].money * t[o].num, s += t[o].num), 
        console.log(a);
        this.setData({
            shop_cart: t,
            totalPrice: a.toFixed(2),
            totalCount: s,
            totalbox: e
        });
    },
    payDesc: function() {
        console.log(this.data);
        var t = parseFloat(this.data.totalPrice), a = parseFloat(this.data.start_at);
        {
            if (2 == this.data.types) return 0 == this.data.totalPrice ? "￥" + this.data.start_at + "元起送" : this.data.totalPrice <= 0 ? "￥" + this.data.start_at + "元起送" : t < a ? (console.log(this.data.totalPrice), 
            "还差" + (a - t).toFixed(2) + "元起送") : (console.log(t), "去结算");
            if (this.data.totalPrice >= 0) return "去下单";
        }
    },
    clear: function(t) {
        this.setData({
            shop_cart: [],
            carArray: [],
            carArray1: [],
            changeHidden: !0
        }), this.calTotalPrice(), this.reload();
    },
    clickImage: function(t) {
        var a = this;
        console.log(t), console.log(a.data);
        a.data.url;
        var e = t.target.dataset.id;
        console.log(e);
        for (var s = [], o = 0; o < a.data.dishes.length; o++) for (var i = 0; i < a.data.dishes[o].goods.length; i++) if (console.log(a.data.dishes[o].goods[i].id), 
        a.data.dishes[o].goods[i].id == e) {
            s.splice(e, 0, a.data.dishes[o].goods[i].img);
            var d = a.data.dishes[o].goods[i];
            app.util.request({
                url: "entry/wxapp/DishesInfo",
                cachetime: "0",
                data: {
                    id: d.id
                },
                success: function(t) {
                    console.log(t.data), wx.navigateTo({
                        url: "../dishinfo/dishinfo?id=" + d.id + "&types=" + a.data.types
                    });
                }
            });
        }
    },
    bomb: function(t) {
        for (var a = t.currentTarget.id, e = this.data.dishes, s = 0, o = e.length; s < o; ++s) for (var i = e[s].goods, d = 0; d < i.length; d++) i[d].id == a ? i[d].open = !i[d].open : i[d].open = !1;
        this.setData({
            dishes: e,
            id: t.currentTarget.id
        });
    },
    jcgwc: function(t) {
        var a = 0;
        for (var e in t) 0 != t[e].num && a++;
        return a;
    },
    pay: function(t) {
        if (console.log(this.data.types), console.log(this.data.shop_cart), 2 == this.data.types) {
            e = this.data.shop_cart;
            if (console.log(this.data.shop_cart), console.log(this.jcgwc(e)), console.log(this.data), 
            wx.setStorageSync("store", this.data.store), wx.setStorageSync("order", this.data.shop_cart), 
            null == e || 0 == e.length) return void wx.showModal({
                title: "提示",
                showCancel: !1,
                content: "请选择菜品"
            });
            if (0 == this.jcgwc(e)) return void wx.showModal({
                title: "提示",
                showCancel: !1,
                content: "请选择菜品"
            });
            if (parseFloat(this.data.totalPrice) < parseFloat(this.data.start_at)) return;
            wx.navigateTo({
                url: "../pay/pay?types=" + this.data.types
            });
        } else {
            var a = this.data.tableid, e = this.data.shop_cart;
            console.log(this.data.shop_cart), console.log(this.data), wx.setStorageSync("store", this.data.store), 
            wx.setStorageSync("order", this.data.shop_cart);
            wx.navigateTo({
                url: "../order/order?types=" + this.data.types + "&tableid=" + a
            });
        }
    },
    navInfo: function(t) {
        wx.switchTab({
            url: "../info/info",
            success: function(t) {},
            fail: function(t) {},
            complete: function(t) {}
        });
    },
    change: function(t) {
        console.log("1111"), this.setData({
            changeHidden: !0
        });
    },
    toastChange: function(t) {
        this.setData({
            toastHidden: !0
        });
    },
    change1: function(t) {
        console.log("1111"), this.setData({
            changeHidden: !1
        });
    },
    ktpay: function() {
        wx.showModal({
            title: "提示",
            content: "此桌已开台不能点菜"
        });
    }
});