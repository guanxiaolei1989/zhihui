var qqmapsdk, app = getApp(), QQMapWX = require("../../utils/qqmap-wx-jssdk.js"), util = require("../../utils/util.js"), pageNum = 1, searchTitle = "", msgListKey = "";

Page({
    data: {
        qqsj: !0,
        msgList: [],
        searchLogList: [],
        hidden: !0,
        scrollTop: 0,
        inputShowed: !1,
        inputVal: "",
        searchLogShowed: !0
    },
    onLoad: function(t) {
        var e = this, o = wx.getStorageSync("imglink");
        wx.getSystemInfo({
            success: function(t) {
                e.setData({
                    windowHeight: t.windowHeight,
                    windowWidth: t.windowWidth,
                    searchLogList: wx.getStorageSync("searchLog") || [],
                    url: o
                });
            }
        }), app.util.request({
            url: "entry/wxapp/system",
            cachetime: "0",
            success: function(t) {
                console.log(t), qqmapsdk = new QQMapWX({
                    key: t.data.map_key
                }), e.setData({
                    mdxx: t.data
                });
            }
        });
    },
    reLoad: function(t) {
        this.setData({
            qqsj: !1
        });
        var e = this;
        wx.getLocation({
            type: "wgs84",
            success: function(o) {
                var a = o.latitude, s = o.longitude, i = a + "," + s;
                console.log(i), qqmapsdk.reverseGeocoder({
                    location: {
                        latitude: a,
                        longitude: s
                    },
                    coord_type: 1,
                    success: function(o) {
                        var a = o.result.ad_info.location;
                        console.log(o), console.log(o.result.formatted_addresses.recommend), console.log("坐标转地址后的经纬度：", o.result.ad_info.location), 
                        e.setData({
                            weizhi: o.result.formatted_addresses.recommend
                        }), app.util.request({
                            url: "entry/wxapp/SearchStore",
                            cachetime: "0",
                            data: {
                                key: t
                            },
                            success: function(t) {
                                console.log(t.data), e.setData({
                                    qqsj: !0
                                }), 0 == t.data.length && e.setData({
                                    tjstorelist: []
                                });
                                for (var o = t.data, s = 0; s < o.length; s++) {
                                    var i = o[s].coordinates.split(",");
                                    console.log(i, a);
                                    var n = util.getDistance(a.lat, a.lng, i[0], i[1]).toFixed(1);
                                    console.log(n), n < 1e3 ? (o[s].aa = n + "m", o[s].aa1 = n) : (o[s].aa = (n / 1e3).toFixed(2) + "km", 
                                    o[s].aa1 = n), e.setData({
                                        tjstorelist: o
                                    });
                                }
                                console.log(o);
                            }
                        });
                    },
                    fail: function(t) {
                        console.log(t);
                    },
                    complete: function(t) {
                        console.log(t);
                    }
                });
            }
        });
    },
    changejwd: function(t, e, o) {
        var a;
        qqmapsdk.reverseGeocoder({
            location: {
                latitude: t,
                longitude: e
            },
            coord_type: 3,
            success: function(t) {
                console.log(t), console.log("坐标转地址后的经纬度：", t.result.ad_info.location), a = t.result.ad_info.location, 
                o(a);
            },
            fail: function(t) {
                console.log(t);
            },
            complete: function(t) {
                console.log(t);
            }
        });
    },
    tzsj: function(t) {
        console.log(t.currentTarget.dataset.sjid);
        var e = t.currentTarget.dataset.sjid;
        getApp().sjid = t.currentTarget.dataset.sjid.id, "0" == e.is_dn && "0" == e.is_pd && "0" == e.is_yy && "1" == e.is_wm && "0" == e.is_sy ? wx.navigateTo({
            url: "../index/index?type=2"
        }) : wx.navigateTo({
            url: "../info/info"
        });
    },
    onReady: function() {},
    onShow: function() {},
    scroll: function(t) {
        this.setData({
            scrollTop: t.detail.scrollTop
        });
    },
    showInput: function() {
        "" != wx.getStorageSync("searchLog") ? this.setData({
            inputShowed: !0,
            searchLogShowed: !0,
            searchLogList: wx.getStorageSync("searchLog")
        }) : this.setData({
            inputShowed: !0,
            searchLogShowed: !0
        });
    },
    searchData: function() {
        console.log(searchTitle);
        if (this.setData({
            msgList: [],
            scrollTop: 0
        }), "" != searchTitle) {
            var t = this.data.searchLogList;
            -1 === t.indexOf(searchTitle) && (t.unshift(searchTitle), wx.setStorageSync("searchLog", t), 
            this.setData({
                searchLogList: wx.getStorageSync("searchLog")
            })), this.reLoad(searchTitle);
        } else wx.showToast({
            title: "搜索内容为空",
            icon: "loading",
            duration: 1e3
        });
    },
    clearInput: function() {
        this.setData({
            msgList: [],
            scrollTop: 0,
            inputVal: ""
        }), searchTitle = "";
    },
    inputTyping: function(t) {
        "" != wx.getStorageSync("searchLog") ? this.setData({
            inputVal: t.detail.value,
            searchLogList: wx.getStorageSync("searchLog")
        }) : this.setData({
            inputVal: t.detail.value,
            searchLogShowed: !0
        }), searchTitle = t.detail.value;
    },
    searchDataByLog: function(t) {
        searchTitle = t.target.dataset.log, console.log(t.target.dataset.log);
        this.setData({
            msgList: [],
            scrollTop: 0,
            inputShowed: !0,
            inputVal: searchTitle
        }), this.searchData();
    },
    clearSearchLog: function() {
        this.setData({
            hidden: !1
        }), wx.removeStorageSync("searchLog"), this.setData({
            scrollTop: 0,
            hidden: !0,
            searchLogList: []
        });
    },
    onHide: function() {},
    onUnload: function() {}
});